import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import json
import dateutil.parser
import time



# mqtt credentials
mqtt_broker_address = "mqtt_address"
mqtt_username = "mqtt_username"
mqtt_password = "mqtt_pass"


# influxdb credentials 
database="ohwairquality"
measurement="AQ_dev2"
influxdbClient = InfluxDBClient(
    host='influx_address', 
    port=8086,username='influx_user', 
    password='influx_password',
    database=database
  )
print(influxdbClient)

# mqtt connection
def on_connect(client, userdata, flags, rc):
  print("Connected with result code {0}".format(str(rc))) 
  client.subscribe("AQ_WTout/#")


# handle messages
def on_message(client, userdata, msg):
  try:
    output=json.loads(msg.payload.decode())
    print(output)
    json_body = [{
      "measurement": measurement,
      "tags":{},
      "fields": output
    }]
    print(json_body)
    influxdbClient.write_points(json_body)
  except:
    pass


client = mqtt.Client("client")
client.username_pw_set(mqtt_username, mqtt_password)

client.on_connect = on_connect
client.on_message = on_message
client.connect(mqtt_broker_address, 1883,60)
client.loop_forever()

